package keriokey.utils;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.http.entity.mime.content.StringBody;
import java.io.BufferedReader;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import org.apache.http.entity.mime.content.ContentBody;
import java.io.InputStream;
import java.nio.charset.Charset;

@SuppressWarnings({"CallToThreadDumpStack", "StaticNonFinalUsedInInitialization"})
public class Utils {

    public static final Random random = new Random();
    private static final String[] ABC_Cirilic = new String[]{
        "й", "ц", "у", "к", "е", "н", "г", "ш", "щ", "з", "х", "ъ", "ф", "ы", "в", "а", "п", "р", "о", "л", "д", "ж", "э", "я", "ч", "с",
        "м", "и", "т", "ь", "б", "ю", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
    };
    private static final String[] ABC_Eanglish = new String[]{
        "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m",
        "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
    };
// The line separator string of the operating system.
    private static final GregorianCalendar calendar = new GregorianCalendar();
    private static final String systemLineSeparator = System.getProperty("line.separator");
// Mapping table from 6-bit nibbles to Base64 characters.
    private static char[] map1 = new char[64];

    static {
        int i = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            map1[i++] = c;
        }
        for (char c = 'a'; c <= 'z'; c++) {
            map1[i++] = c;
        }
        for (char c = '0'; c <= '9'; c++) {
            map1[i++] = c;
        }
        map1[i++] = '+';
        map1[i++] = '/';
    }
// Mapping table from Base64 characters to 6-bit nibbles.
    private static byte[] map2 = new byte[128];

    static {
        for (int i = 0; i < map2.length; i++) {
            map2[i] = -1;
        }
        for (int i = 0; i < 64; i++) {
            map2[map1[i]] = (byte) i;
        }
    }

    /**
     * Encodes a string into Base64 format.
     * No blanks or line breaks are inserted.
     * @param s  A String to be encoded.
     * @return   A String containing the Base64 encoded data.
     */
    public synchronized static String encodeString(String s) {
        return new String(encode(s.getBytes()));
    }

    /**
     * Encodes a byte array into Base 64 format and breaks the output into lines of 76 characters.
     * This method is compatible with <code>sun.misc.BASE64Encoder.encodeBuffer(byte[])</code>.
     * @param in  An array containing the data bytes to be encoded.
     * @return    A String containing the Base64 encoded data, broken into lines.
     */
    public static String encodeLines(byte[] in) {
        return encodeLines(in, 0, in.length, 76, systemLineSeparator);
    }

    /**
     * Encodes a byte array into Base 64 format and breaks the output into lines.
     * @param in            An array containing the data bytes to be encoded.
     * @param iOff          Offset of the first byte in <code>in</code> to be processed.
     * @param iLen          Number of bytes to be processed in <code>in</code>, starting at <code>iOff</code>.
     * @param lineLen       Line length for the output data. Should be a multiple of 4.
     * @param lineSeparator The line separator to be used to separate the output lines.
     * @return              A String containing the Base64 encoded data, broken into lines.
     */
    public static String encodeLines(byte[] in, int iOff, int iLen, int lineLen, String lineSeparator) {

        int blockLen = (lineLen * 3) / 4;
        if (blockLen <= 0) {
            throw new IllegalArgumentException();
        }
        int lines = (iLen + blockLen - 1) / blockLen;
        int bufLen = ((iLen + 2) / 3) * 4 + lines * lineSeparator.length();
        StringBuilder buf = new StringBuilder(bufLen);
        int ip = 0;
        while (ip < iLen) {
            int l = Math.min(iLen - ip, blockLen);
            buf.append(encode(in, iOff + ip, l));
            buf.append(lineSeparator);
            ip += l;
        }

        return buf.toString();
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted in the output.
     * @param in  An array containing the data bytes to be encoded.
     * @return    A character array containing the Base64 encoded data.
     */
    public synchronized static char[] encode(byte[] in) {
        return encode(in, 0, in.length);
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted in the output.
     * @param in    An array containing the data bytes to be encoded.
     * @param iLen  Number of bytes to process in <code>in</code>.
     * @return      A character array containing the Base64 encoded data.
     */
    public synchronized static char[] encode(byte[] in, int iLen) {
        return encode(in, 0, iLen);
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted in the output.
     * @param in    An array containing the data bytes to be encoded.
     * @param iOff  Offset of the first byte in <code>in</code> to be processed.
     * @param iLen  Number of bytes to process in <code>in</code>, starting at <code>iOff</code>.
     * @return      A character array containing the Base64 encoded data.
     */
    public static char[] encode(byte[] in, int iOff, int iLen) {

        int oDataLen = (iLen * 4 + 2) / 3;       // output length without padding
        int oLen = ((iLen + 2) / 3) * 4;         // output length including padding
        char[] out = new char[oLen];
        int ip = iOff;
        int iEnd = iOff + iLen;
        int op = 0;
        while (ip < iEnd) {
            int i0 = in[ip++] & 0xff;
            int i1 = ip < iEnd ? in[ip++] & 0xff : 0;
            int i2 = ip < iEnd ? in[ip++] & 0xff : 0;
            int o0 = i0 >>> 2;
            int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
            int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
            int o3 = i2 & 0x3F;
            out[op++] = map1[o0];
            out[op++] = map1[o1];
            out[op] = op < oDataLen ? map1[o2] : '=';
            op++;
            out[op] = op < oDataLen ? map1[o3] : '=';
            op++;

        }
        return out;
    }

    /**
     * Decodes a string from Base64 format.
     * No blanks or line breaks are allowed within the Base64 encoded input data.
     * @param s  A Base64 String to be decoded.
     * @return   A String containing the decoded data.
     * @throws   IllegalArgumentException If the input is not valid Base64 encoded data.
     */
    public synchronized static String decodeString(String s) {
        return new String(decode(s));
    }

    /**
     * Decodes a byte array from Base64 format and ignores line separators, tabs and blanks.
     * CR, LF, Tab and Space characters are ignored in the input data.
     * This method is compatible with <code>sun.misc.BASE64Decoder.decodeBuffer(String)</code>.
     * @param s  A Base64 String to be decoded.
     * @return   An array containing the decoded data bytes.
     * @throws   IllegalArgumentException If the input is not valid Base64 encoded data.
     */
    public static byte[] decodeLines(String s) {
        char[] buf = new char[s.length()];
        int p = 0;
        for (int ip = 0; ip < s.length(); ip++) {
            char c = s.charAt(ip);
            if (c != ' ' && c != '\r' && c != '\n' && c != '\t') {
                buf[p++] = c;
            }
        }
        return decode(buf, 0, p);
    }

    /**
     * Decodes a byte array from Base64 format.
     * No blanks or line breaks are allowed within the Base64 encoded input data.
     * @param s  A Base64 String to be decoded.
     * @return   An array containing the decoded data bytes.
     * @throws   IllegalArgumentException If the input is not valid Base64 encoded data.
     */
    public synchronized static byte[] decode(String s) {
        return decode(s.toCharArray());
    }

    /**
     * Decodes a byte array from Base64 format.
     * No blanks or line breaks are allowed within the Base64 encoded input data.
     * @param in  A character array containing the Base64 encoded data.
     * @return    An array containing the decoded data bytes.
     * @throws    IllegalArgumentException If the input is not valid Base64 encoded data.
     */
    public static byte[] decode(char[] in) {
        return decode(in, 0, in.length);
    }

    /**
     * Decodes a byte array from Base64 format.
     * No blanks or line breaks are allowed within the Base64 encoded input data.
     * @param in    A character array containing the Base64 encoded data.
     * @param iOff  Offset of the first character in <code>in</code> to be processed.
     * @param iLen  Number of characters to process in <code>in</code>, starting at <code>iOff</code>.
     * @return      An array containing the decoded data bytes.
     * @throws      IllegalArgumentException If the input is not valid Base64 encoded data.
     */
    public synchronized static byte[] decode(char[] in, int iOff, int iLen) {
        if (iLen % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (iLen > 0 && in[iOff + iLen - 1] == '=') {
            iLen--;
        }
        int oLen = (iLen * 3) / 4;
        byte[] out = new byte[oLen];
        int ip = iOff;
        int iEnd = iOff + iLen;
        int op = 0;
        while (ip < iEnd) {
            int i0 = in[ip++];
            int i1 = in[ip++];
            int i2 = ip < iEnd ? in[ip++] : 'A';
            int i3 = ip < iEnd ? in[ip++] : 'A';
            if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int b0 = map2[i0];
            int b1 = map2[i1];
            int b2 = map2[i2];
            int b3 = map2[i3];
            if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int o0 = (b0 << 2) | (b1 >>> 4);
            int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
            int o2 = ((b2 & 3) << 6) | b3;
            out[op++] = (byte) o0;
            if (op < oLen) {
                out[op++] = (byte) o1;
            }
            if (op < oLen) {
                out[op++] = (byte) o2;
            }
        }
        return out;
    }

    public static void wtiteFile(String content, String name, String format) {
        try {
            File f = new File(name + "." + format);
            f.delete();
            FileWriter fr = new FileWriter(name + "." + format);
            BufferedWriter bw = new BufferedWriter(fr);
            bw.write(content);
            bw.close();
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void wtiteFile(byte[] b, String name, String format) {
        try {
            File f = new File(name + "." + format);
            f.delete();
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(b);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getTime(String separator) {
        calendar.setTime(new Date());
        return calendar.get(GregorianCalendar.HOUR_OF_DAY) + separator + calendar.get(GregorianCalendar.MINUTE) + separator + calendar.get(GregorianCalendar.SECOND);
    }

    public static void saveLog(String log) {
        wtiteFile(log, "./Error." + getTime(".") + "_" + System.currentTimeMillis(), "log");
    }

    public static void print(BufferedReader br) {

        System.out.println(br2str(br));
    }

    public static String br2str(BufferedReader br) {
        try {
            String s = "", s1 = "";
            while ((s = br.readLine()) != null) {
                s1 += s;
            }
            return s;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getRandomCirilicString(int count) {
        if (count < 1) {
            return "";
        } else {
            String tmp = "";
            for (int i = 0; i < count; i++) {
                tmp += ABC_Cirilic[random.nextInt(42)];
            }
            return tmp;
        }
    }

    public static String getRandomEanglishString(int count) {
        if (count < 1) {
            return "";
        } else {
            String tmp = "";
            for (int i = 0; i < count; i++) {
                tmp += ABC_Eanglish[random.nextInt(36)];
            }
            return tmp;
        }
    }

    public static ContentBody sb(String s) {

        StringBody sb = null;
        if (s == null) {
            return (ContentBody) sb;
        }
        try {
            sb = new StringBody(s, Charset.forName("utf-8"));
        } catch (Exception uee) {
            uee.printStackTrace();
        }
        return (ContentBody) sb;
    }

    public static String genHostId() {

        double h1 = random.nextInt(90);
        double h2 = random.nextInt(90);
        double h3 = random.nextInt(90);
        double h4 = random.nextInt(90);
        double h5 = random.nextInt(90);
        double h6 = random.nextInt(90);

        return (h1 < 10 ? "0" + h1 : h1) + ":"
                + (h2 < 10 ? "0" + h2 : h2) + ":"
                + (h3 < 10 ? "0" + h3 : h3) + ":"
                + (h4 < 10 ? "0" + h4 : h4) + ":"
                + (h5 < 10 ? "0" + h5 : h5) + ":"
                + (h6 < 10 ? "0" + h6 : h6);
    }

    public static int getYEAR() {
        return calendar.get(GregorianCalendar.YEAR);
    }

    public static int getMONTH() {
        return calendar.get(GregorianCalendar.MONTH);
    }

    public static int getDATE() {
        return calendar.get(GregorianCalendar.DAY_OF_MONTH);
    }

    public static byte[] in2byte(InputStream in) {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = in.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();
            return buffer.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTime() {
        calendar.setTime(new Date());
        int hour = calendar.get(GregorianCalendar.HOUR_OF_DAY);
        int minute = calendar.get(GregorianCalendar.MINUTE);
        int second = calendar.get(GregorianCalendar.SECOND);
        return "[" + (hour < 10 ? "0" + hour : hour) + "." + (minute < 10 ? "0" + minute : minute) + "." + (second < 10 ? "0" + second : second) + "]";
    }
} // end class utils

