/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package keriokey.utils;


import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import javax.imageio.ImageIO;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.GraphicsDevice;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.PixelGrabber;
import java.awt.Transparency;
import java.awt.HeadlessException;
/**
 *
 * @author Hanyuu Furude
 */
public class CaptchaUtils {

    public static BufferedImage toBufferedImage(Image image) {
        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        }

        // This code ensures that all the pixels in the image are loaded
        image = new ImageIcon(image).getImage();

        // Determine if the image has transparent pixels
        boolean hasAlpha = hasAlpha(image);

        // Create a buffered image with a format that's compatible with the screen
        BufferedImage bimage = null;
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
            // Determine the type of transparency of the new buffered image
            int transparency = Transparency.OPAQUE;
            if (hasAlpha == true) {
                transparency = Transparency.BITMASK;
            }

            // Create the buffered image
            GraphicsDevice gs = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gs.getDefaultConfiguration();
            bimage = gc.createCompatibleImage(image.getWidth(null), image.getHeight(null), transparency);
        } catch (HeadlessException e) {
        } //No screen

        if (bimage == null) {
            // Create a buffered image using the default color model
            int type = BufferedImage.TYPE_INT_RGB;
            if (hasAlpha == true) {
                type = BufferedImage.TYPE_INT_ARGB;
            }

            bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
        }

        // Copy image to buffered image
        Graphics g = bimage.createGraphics();

        // Paint the image onto the buffered image
        g.drawImage(image, 0, 0, null);
        g.dispose();

        return bimage;
    }

    public static boolean hasAlpha(Image image) {
        // If buffered image, the color model is readily available
        if (image instanceof BufferedImage) {
            return ((BufferedImage) image).getColorModel().hasAlpha();
        }

        // Use a pixel grabber to retrieve the image's color model;
        // grabbing a single pixel is usually sufficient
        PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
        try {
            pg.grabPixels();
        } catch (InterruptedException e) {
        }

        // Get the image's color model
        return pg.getColorModel().hasAlpha();
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public static byte[] getScaledCaptcha(double scaleW, double scaleH, byte[] cap) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int w, h;
            ByteArrayInputStream bais=new ByteArrayInputStream(cap);
            Image c = ImageIO.read(bais);
            w = (int) Math.round(c.getWidth(null) * (scaleW <1 ? 1 : scaleW));
            h = (int) Math.round(c.getHeight(null) * (scaleH <1 ? 1 : scaleH));
            c = c.getScaledInstance(w, h, 1);
            BufferedImage bi = toBufferedImage(c);
            bi=inverse(bi);
            ImageIO.write(bi, "png", out);
            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static BufferedImage inverse(BufferedImage bi)
    {
        int _w=bi.getWidth();
        int _h=bi.getHeight();
        for(int x=0;x<_w;x++)
        {
            for(int y=0;y<_h;y++)
            {
                bi.setRGB(x, y, (bi.getRGB(x, y)^0xffffff));
            }
        }
        return bi;
    }
}
