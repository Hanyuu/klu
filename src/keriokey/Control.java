/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package keriokey;

import keriokey.utils.CaptchaUtils;
import keriokey.utils.Utils;
import keriokey.ui.GUI;
import http.HTTPClient;
import http.HttpProxy;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.http.HttpHost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.entity.mime.content.ContentBody;
import java.nio.charset.Charset;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.ByteArrayOutputStream;
import org.apache.http.entity.mime.content.StringBody;
import java.io.BufferedReader;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import http.HttpProxy;
import java.util.StringTokenizer;
import java.awt.Image;

/**
 *
 * @author Hanyuu
 */
public class Control {

    private HTTPClient client;
    private GUI gui;
    private static Control _instance;
    private String host_id = "";
    private String token = "";
    private boolean isError = false;

    public static void init() {
        _instance = new Control();
    }

    public Control() {

        gui = new GUI(this);
        gui.setVisible(true);
        host_id = Utils.genHostId();
    }

    //1
    public void connect(String proxy) {
        try {
            this.isError = false;
            if (proxy.isEmpty()) {
                client = new HTTPClient(this);
            } else {
                StringTokenizer st = new StringTokenizer(proxy, ":");
                HttpProxy p = new HttpProxy(st.nextToken(), Integer.parseInt(st.nextToken()));
                client = new HTTPClient(p, this);
            }
            client.setControl(this);
            log("[+]Connecting...", false);
            MultipartEntity me = new MultipartEntity();
            me.addPart("command", Utils.sb("connect"));
            me.addPart("host_id", Utils.sb(host_id));
            me.addPart("product_code", Utils.sb("KWF"));
            me.addPart("type", Utils.sb("image/png"));
            me.addPart("protocol_version", Utils.sb("6"));
            me.addPart("lang_id", Utils.sb("en"));
            BufferedReader br = client.postBufferedReader("https://register.kerio.com/registration/LD.php", me);

            byte[] cap = Utils.decode(parseImgData(br));
            ImageIcon im = new ImageIcon(CaptchaUtils.getScaledCaptcha(2, 1.5, cap));

            gui.setCap(im);
        } catch (Exception e) {
            e.printStackTrace();
            gui.setCap(null);
            log("Error in \"connect()\"\n" + e.toString(), true);
        }
    }

    //2
    public void lookup(String cap) {
        try {
            log("[+]Sending CAPTCHA code...", false);

            MultipartEntity me = new MultipartEntity();
            me.addPart("command", Utils.sb("lookup"));
            me.addPart("token", Utils.sb(token));
            me.addPart("base_id", Utils.sb("TRIAL"));

            me.addPart("host_id", Utils.sb(host_id));
            me.addPart("promo_id", Utils.sb(""));

            me.addPart("product_code", Utils.sb("KWF"));
            me.addPart("security_code", Utils.sb(cap));

            me.addPart("protocol_version", Utils.sb("6"));
            me.addPart("os", Utils.sb("1"));
            client.postBufferedReader("https://register.kerio.com/registration/LD.php", me);
            if (!isError) {
                create();
            }

        } catch (Exception e) {
            e.printStackTrace();
            log("Error in \"lookup()\"\n" + e, true);
        }
    }
    //3

    private void create() {
        try {
            log("[+]Registering trial...", false);

            MultipartEntity me = new MultipartEntity();

            me.addPart("command", Utils.sb("create"));
            me.addPart("base_id", Utils.sb("TRIAL"));
            me.addPart("trial_id", Utils.sb(""));
            me.addPart("promo_id", Utils.sb(""));
            me.addPart("token", Utils.sb(token));
            me.addPart("host_id", Utils.sb(host_id));
            me.addPart("token", Utils.sb(token));
            me.addPart("product_code", Utils.sb("KWF"));
            me.addPart("os", Utils.sb("1"));


            me.addPart("adress", Utils.sb(Utils.getRandomCirilicString(10)));
            me.addPart("city", Utils.sb(Utils.getRandomCirilicString(10)));
            me.addPart("comment", Utils.sb(""));
            me.addPart("company", Utils.sb(Utils.getRandomCirilicString(10)));
            me.addPart("country", Utils.sb("TJ"));
            me.addPart("email", Utils.sb(Utils.getRandomEanglishString(4) + "@" + Utils.getRandomEanglishString(4) + ".com"));
            me.addPart("person", Utils.sb(Utils.getRandomCirilicString(10)));
            me.addPart("phone", Utils.sb(String.valueOf(Utils.random.nextInt(20000))));

            // me.addPart("q1", Utils.sb("1"));  //WTF?
            //me.addPart("q2", Utils.sb("6"));  //WTF?

            me.addPart("state", Utils.sb(""));

            int m = Utils.getMONTH();
            int y = Utils.getYEAR();
            int d = Utils.getDATE();

            if (m == 12) {
                y++;
                m = 1;
            } else {
                m++;
            }
            d++;
            me.addPart("trial_expires", Utils.sb(y + "-" + m + "-" + d));
            me.addPart("website", Utils.sb(""));
            me.addPart("zipcode", Utils.sb(String.valueOf(Utils.random.nextInt(50000))));
            BufferedReader br = client.postBufferedReader("https://register.kerio.com/registration/LD.php", me);
            if (!isError) {
                parseTrialId(br);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log("Error in \"create()\"\n" + e, true);
        }
    }

    public void log(String msg, boolean isError) {
        if (!this.isError) {
            this.isError = isError;
        }
        System.out.println(msg);
        gui.log(msg);
        if (isError) {
            gui.alertError(msg);
        }

    }

    private String parseImgData(BufferedReader br) throws Exception {
        String s = "", s2 = "";
        br.readLine();
        while ((s = br.readLine()) != null) {
            if (!s.isEmpty()) {
                s2 += s;
            } else {
                return s2;
            }
        }
        return "";

    }

    public void setToken(String token) {
        this.token = token;
    }

    private void parseTrialId(BufferedReader br) {
        try {

            String trial_id = br.readLine().replaceAll("trial_id: ", "").trim();
            log("[+]You trial ID is: " + trial_id, false);
            gui.setTrialKey(trial_id);
            /*log("[+]Ask for Web-Filter trial key.", false);
            String web_key = client.getBufferedReader("https://esoftsgs.kerio.com/getkey.php?id=" + trial_id).readLine();
            log("[+]Web-Filter key is: " + web_key, false);
            gui.setWebKey(web_key);*/

        } catch (Exception e) {
            e.printStackTrace();
            log("Error in \"parseTrialId()\"\n" + e, true);
        }


    }

    private String[] parseSnortReply(BufferedReader br) {
        String[] result=new String[2];
        try {
            String str="";
            while((str=br.readLine())!=null){
                if(str.startsWith("0:"))
                    result[0]=str.replace("0:","");
                if(str.startsWith("full:"))
                    result[1]=str.replace("full:","");
            }
            if(result[1]==null)
                gui.alertInfo("You use the latest version.");
            
        } catch (Exception e) {
            e.printStackTrace();
            log("Error in \"parseTrialId()\"\n" + e, true);
        }
        return result;
    }

    public void updateSnort(String id, String ver) {
        log("[+]Requing snort updates url...",false);
        BufferedReader br = client.getBufferedReader("https://control-update.kerio.com/update.php?id=" + id + "&version=" + ver);
        String[] result=parseSnortReply(br);
        String version=result[0];
        String updUrl=result[1];
        log("[+]Updates url is: "+updUrl+"\n[+]Version is: "+version,false);
        InputStream in=client.getInputStream(updUrl);
        byte[] update=Utils.in2byte(in);
        Utils.wtiteFile(update, version, "gz");
        
    }
}
