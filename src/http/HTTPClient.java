/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package http;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.ArrayList;
import org.apache.http.HttpVersion;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.params.HttpParams;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLSocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.TrustManager;
import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import keriokey.Control;

/**
 *
 * @author Hanyuu Furude
 */
public class HTTPClient {

    private String proxyHost, proxyLogin, proxyPassword;
    private int proxyPort = -1;
    //private Wipe wipe=null;
    private String cookies;
    private HttpHost proxy = null;
    //private UI ui;
    private Control control;
    public HTTPClient(HttpProxy proxy,Control control) {
        this.proxyHost = proxy.getHost();
        this.proxyPort = proxy.getPort();
        this.proxy = proxy.getProxy();
        this.control=control;
        if (proxy.isAuth()) {
            this.proxyLogin = proxy.getLogin();
            this.proxyPassword = proxy.getPassword();
        }
        control.log("[+]Connect via proxy "+proxy+".", false);  
    }
    public HTTPClient(Control control){
        this.control=control;
     control.log("[+]Direct connect.", false);  
    }
    public void setControl(Control control){
        this.control=control;
    }

    /**
     *
     * @param RequestUrl
     * @param host
     * @para headers
     * @param saveCookies
     * @return
     */
    @SuppressWarnings("CallToThreadDumpStack")
    public BufferedReader getBufferedReader(String RequestUrl, HttpHost host, ArrayList<String> headers, boolean saveCookies) {
        try {
            return new BufferedReader(new InputStreamReader(getInputStream(RequestUrl, host, headers, saveCookies), "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param RequestUrl
     * @param saveCookies
     * @return
     */
    public BufferedReader getBufferedReader(String RequestUrl, boolean saveCookies) {
        return getBufferedReader(RequestUrl, null, null, saveCookies);
    }

    /**
     *
     * @param RequestUrl
     * @return
     */
    public BufferedReader getBufferedReader(String RequestUrl) {
        return getBufferedReader(RequestUrl, null, null, false);
    }

    /**
     *
     * @param RequestUrl
     * @param headers
     * @return
     */
    public BufferedReader getBufferedReader(String RequestUrl, ArrayList<String> headers) {
        return getBufferedReader(RequestUrl, null, headers, false);
    }

    /**
     *
     * @param RequestUrl
     * @param save cookies
     * @return
     */
    public InputStream getInputStream(String RequestUrl, boolean saveCookies) {
        return getInputStream(RequestUrl, null, null, saveCookies);
    }

    /**
     *
     * @param RequestUrl
     * @return
     */
    public InputStream getInputStream(String RequestUrl) {
        return getInputStream(RequestUrl, null, null, false);
    }

    /**
     *
     * @param RequestUrl
     * @param host
     * @param headers
     * @param save cookies
     * @return
     */
    @SuppressWarnings("CallToThreadDumpStack")
    public InputStream getInputStream(String RequestUrl, HttpHost host, ArrayList<String> headers, boolean saveCookies) {
        try {
            HttpGet get = new HttpGet(RequestUrl);
            HttpResponse hp;
            if (headers != null) {
                for (String header : headers) {
                    StringTokenizer st = new StringTokenizer(header, ",");
                    get.addHeader(st.nextToken(), st.nextToken());
                }
            }

            if (host == null) {
                hp = createClient().execute(get);
            } else {
                hp = createClient().execute(host, get);
            }

            if (saveCookies) {
                saveCookies(hp);
            }

            InputStream result = hp.getEntity().getContent();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveCookies(HttpResponse hp) {
        cookies = "";
        for (Header hh : hp.getAllHeaders()) {
            if (hh.getName().contains("ookie")) {
                //System.out.println("save:"+hh.getName()+":"+hh.getValue());
                String tmp = hh.getValue();
                if (tmp.endsWith(";")) {
                    cookies += tmp;
                } else {
                    cookies += tmp + ";";
                }
                tmp = null;
            }
        }
    }

    public void addCookie(String cookie) {
        if (cookie.endsWith(";")) {
            cookies += cookie;
        } else {
            cookies += cookie + ";";
        }
    }

    public String getCookieByName(String name) {
        StringTokenizer st = new StringTokenizer(cookies, ";");
        while (st.hasMoreTokens()) {
            String s = st.nextToken();
            if (s.startsWith(name)) {
                StringTokenizer ct = new StringTokenizer(s, "=");
                ct.nextToken();
                return ct.nextToken();
            }
        }
        return "";
    }

    public String getCookies() {
        return cookies;
    }

    /**
     *
     * @param url
     * @param host
     * @param me
     * @return
     */
    public BufferedReader postBufferedReader(String url, HttpHost host, MultipartEntity me) {
        return postBufferedReader(url, host, me, null);
    }

    /**
     *
     * @param url
     * @param me
     * @return
     */
    public BufferedReader postBufferedReader(String url, MultipartEntity me) {
        return postBufferedReader(url, null, me, null);
    }

    /**
     *
     * @param url
     * @param me
     * @para headers
     * @return
     */
    public BufferedReader postBufferedReader(String url, MultipartEntity me, ArrayList<String> headers) {
        return postBufferedReader(url, null, me, headers);
    }

    /**
     *
     * @param url
     * @param host
     * @param me
     * @param headers
     * @return
     */
    public BufferedReader postBufferedReader(String url, HttpHost host, MultipartEntity me, ArrayList<String> headers) {
        try {
            return new BufferedReader(new InputStreamReader(post(url, me, headers), "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param url
     * @param me
     * @return
     */
    public InputStream post(String url, MultipartEntity me) {
        return post(url, null, me, null);
    }

    /**
     *
     * @param url
     * @param me
     * @param headers
     * @return
     */
    public InputStream post(String url, MultipartEntity me, ArrayList<String> headers) {
        return post(url, null, me, headers);
    }

    public BufferedReader postBufferedReaderURLENCODED(String url, HttpHost host, String value, ArrayList<String> headers) {
        try {
            return new BufferedReader(new InputStreamReader(postURLENCODED(url, host, value, headers), "UTF-8"));
        } catch (Exception e) {
        }
        return null;
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public InputStream postURLENCODED(String url, HttpHost host, String value, ArrayList<String> sheaders) {
        try {

            HttpPost post = new HttpPost(url);
            if (sheaders != null) {
                for (String header : sheaders) {
                    StringTokenizer st = new StringTokenizer(header, ",");
                    post.addHeader(st.nextToken(), st.nextToken());
                }
            }

            StringEntity postEntity = new StringEntity(value, "UTF-8");
            postEntity.setContentType("application/x-www-form-urlencoded");
            post.setEntity(postEntity);

            HttpResponse hp;
            if (host == null) {
                hp = createClient().execute(post);
            } else {
                hp = createClient().execute(host, post);
            }

            saveCookies(hp);
            for (Header h : hp.getAllHeaders()) {
                p(h.getName() + ':' + h.getValue());
            }
            return hp.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void p(Object o) {
        System.out.println(o);
    }

    /**
     *
     * @param url
     * @param host
     * @param me
     * @param String headers
     * @return
     */
    @SuppressWarnings("CallToThreadDumpStack")
    public InputStream post(String url, HttpHost host, MultipartEntity me, ArrayList<String> sheaders) {
        try {
            HttpPost post = new HttpPost(url);

            if (sheaders != null) {
                for (String header : sheaders) {
                    StringTokenizer st = new StringTokenizer(header, ",");
                    post.addHeader(st.nextToken(), st.nextToken());
                }
            }
            if (cookies != null) {

                post.addHeader("Cookie", cookies);
                cookies = "";
            }

            post.setEntity(me);
            HttpResponse hp;

            if (host == null) {
                hp = createClient().execute(post);
            } else {
                hp = createClient().execute(host, post);
            }
            saveCookies(hp);


            InputStream result = hp.getEntity().getContent();
            Header token = hp.getFirstHeader("X-Kerio-Token");
            Header code = hp.getFirstHeader("X-Kerio-Reply-Code");
            
            if (code != null && Integer.parseInt(code.getValue()) != 200) {
                control.log("X-Kerio-Reply-Code : "+code.getValue()+"\n"+
                                    "X-Kerio-Reply-Message : "+hp.getFirstHeader("X-Kerio-Reply-Message").getValue(),true);
                control.log("\n[!]ERROR! All reaponse data will print\n",true);
                for (Header hh : hp.getAllHeaders()) {
                    p(hh.getName() + ":" + hh.getValue());
                }
            }
            control.log("[+]"+code.getName()+" is "+code.getValue()+".",false);
            if (token != null) {
                control.log("[+]"+token.getName()+" is "+token.getValue(),false);
                control.setToken(token.getValue());
            }


            return result;
        } catch (Exception e) {
            control.log("Connection problem.\n"+e.toString(),true);
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @return true if proxy enable
     */
    public boolean isViaProxy() {
        return proxyHost != null && proxyPort > 0;
    }

    /**
     *
     * @return
     */
    public boolean isProxyAuth() {
        return proxyLogin != null && proxyPassword != null;
    }

    /**
     * Create HTTP Client
     * @return
     */
    @SuppressWarnings("CallToThreadDumpStack")
    protected final DefaultHttpClient createClient() {
        DefaultHttpClient httpclient = null;
        try {
            SchemeRegistry supportedSchemes = new SchemeRegistry();

            supportedSchemes.register(new Scheme("http",
                    PlainSocketFactory.getSocketFactory(), 80));

            SSLSocketFactory ssf = new SSLSocketFactory(createSSLContext());
            ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            supportedSchemes.register(new Scheme("https",
                    ssf, 443));
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setUserAgent(params, "");
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            HttpProtocolParams.setUseExpectContinue(params, false);

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params,
                    supportedSchemes);
            httpclient = new DefaultHttpClient(ccm, params);
            if (isViaProxy()) {
                httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
            }
            if (isProxyAuth()) {
                httpclient.getCredentialsProvider().setCredentials(
                        new AuthScope(proxyHost, proxyPort),
                        new UsernamePasswordCredentials(proxyLogin, proxyPassword));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wrapClient(httpclient);
    }

    private SSLContext createSSLContext() throws KeyManagementException, NoSuchAlgorithmException {
        SSLContext ctx = SSLContext.getInstance("TLS");
            X509TrustManager tm = new X509TrustManager() {

                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType)
                        throws java.security.cert.CertificateException {
                    // TODO Auto-generated method stub

                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType)
                        throws java.security.cert.CertificateException {
                    // TODO Auto-generated method stub

                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    // TODO Auto-generated method stub
                    return null;
                }
            };
            ctx.init(null, new TrustManager[] { tm }, null);
        return ctx;
    }
    
    public static DefaultHttpClient wrapClient(DefaultHttpClient base) {
        try {
            SSLContext ctx = SSLContext.getInstance("SSL");
            X509TrustManager tm = new X509TrustManager() {

                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType)
                        throws java.security.cert.CertificateException {
                    // TODO Auto-generated method stub

                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType)
                        throws java.security.cert.CertificateException {
                    // TODO Auto-generated method stub

                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    // TODO Auto-generated method stub
                    return null;
                }
            };
            ctx.init(null, new TrustManager[] { tm }, null);
            SSLSocketFactory ssf = new SSLSocketFactory(ctx);
            ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            ClientConnectionManager ccm = base.getConnectionManager();
            SchemeRegistry sr = ccm.getSchemeRegistry();
            sr.register(new Scheme("https", ssf, 443));
            return new DefaultHttpClient(ccm, base.getParams());
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
