/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package http;
import  org.apache.http.HttpHost;
/**
 *
 * @author Hanyuu Furude
 */
public class HttpProxy
{
    private String host;
    private int port;
    private String login;
    private String password;
    private HttpHost ProxyHost;

    public HttpProxy (String host,int port)
    {
        this.host=host;
        this.port=port;
        this.ProxyHost=new HttpHost(this.host, this.port, "http");
    }

    public HttpProxy (String host,int port,String login,String password)
    {
        this.host=host;
        this.port=port;
        this.login=login;
        this.password=password;
        this.ProxyHost=new HttpHost(this.host, this.port, "http");
    }

    public boolean isAuth()
    {
        return (login!=null);
    }

    public String toSipleString()
    {
        return host+":"+port;
    }

    public String getHost()
    {
        return host;
    }

    public String getLogin()
    {
        return login;
    }

    public String getPassword()
    {
        return password;
    }

    public int getPort()
    {
        return port;
    }

    public HttpHost getProxy()
    {
        return ProxyHost;
    }

    @Override
    public String toString()
    {
        if(isAuth())
        {
            return host+":"+port+":"+login+":"+password;
        }
        else
        {
            return host+":"+port;
        }
    }
}
